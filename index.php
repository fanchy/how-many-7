<?php 

	// PHP params
	ini_set('memory_limit', '-1');
	// ini_set('memory_limit', '32M');

	// Page params
	$meta_title  = "Combien de fois le chiffre 7 jusqu'à 1000 ? | Fanchy Fanch";
	$meta_desc	 = "Combien de fois retrouve-t-on le chiffre 7 (ou autre) jusqu'à 1000 (ou autre) ? Voilà un algorithme qui va vous permettre de répondre à cette question facilement !";
	$meta_locale = 'fr_FR';

	// Custom params
	$_min	= 0;
	$_max 	= 999999;	// PHP_INT_MAX;

	$min 	= (isset($_GET['min']) && filter_var($_GET['min'], FILTER_VALIDATE_INT)) ? $_GET['min'] : 0;
	$max 	= (isset($_GET['max']) && filter_var($_GET['max'], FILTER_VALIDATE_INT)) ? $_GET['max'] : 1000;
	$query	= (isset($_GET['query']) && filter_var($_GET['query'], FILTER_VALIDATE_INT)) ? $_GET['query'] : 7;
	
	// Correct user entry if needed
	if($min < $_min) $min 	= $_min;
	if($max > $_max) $max 	= $_max;
	if($query < 0)	 $query	= 0;
	if($query > 9)	 $query	= 9;

	// $max 	= '1';
	// $max   .= '0';
	// $max   .= '0';
	// $max   .= '000';
	// $max   .= '000';
	// $max   .= '000';
	$max 	= (int)$max;
	// $max 	= (double)$max;

	// Default script params
	$class  = ['simples', 'milliers', 'millions', 'milliards'];
	$units  = ['unités', 'dizaines', 'centaines'];
	$subst	= 'X';
	$total  = array_combine(array_reverse(array_keys(str_split($max))), array_fill(0, strlen($max), 0));
	$final	= 0;
	$result = [];

	// Start calculating results
	for($i = $min; $i < $max; $i++){
		// Set current variable
		$current = str_pad($i, strlen($max), 'X', STR_PAD_LEFT);

		// Feed final array with current occurences of query
		foreach(str_split(strrev($current)) as $key => $val){
			if($val == $query){
				$result[$i][$key] = 1;
				$total[$key]++;
				$final++;
			}else{
				$result[$i][$key] = 0;
			}
		}

		// Revert array order
		krsort($result[$i], SORT_DESC);
	}
	

	// echo '<pre>';
	// var_dump(
	// 	PHP_INT_MAX,
	// 	$max,
	// 	strrev(implode(' ', str_split((string)strrev($max), 3))),
	// 	filter_var($max, FILTER_VALIDATE_INT),
	// 	filter_var($max, FILTER_VALIDATE_FLOAT),
	// 	// $total,
	// 	array_keys($total)
	// 	// $result
	// );
	// echo '</pre>';
	// die('<hr />');

?>
<!doctype html>
<html class="no-js" lang="<?php echo $meta_locale; ?>">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="x-ua-compatible" content="ie=edge" />
		<title><?php echo $meta_title; ?></title>
		<meta name="description" content="<?php echo $meta_desc; ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />

		<link rel="shortcut icon" href="http://www.fanchy.fr/favicon.ico" />
		<link rel='https://api.w.org/' href='http://www.fanchy.fr/wp-json/' />

		<meta property="og:url"                content="http://fanchy.fr/wp-content/uploads/how-many-7/" />
		<meta property="og:type"               content="website" />
		<meta property="og:title"              content="<?php echo $meta_title; ?>" />
		<meta property="og:description"        content="<?php echo $meta_desc; ?>" />
		<meta property="og:image"              content="http://www.fanchy.fr/wp-content/uploads/Logo-Fanchy-Fanch-300x300.png" />

		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

		<style>
			/*--------------------------------------------------------
				GENERIC STUFFS
			--------------------------------------------------------*/
			html{
				position:relative;
				min-height:100%;
			}
			
			body{
				margin-top:60px;
				margin-bottom:60px;
			}

			a{text-decoration:none !important; }

			label{cursor:pointer; }

			/*--------------------------------------------------------
				STICKY FOOTER
			--------------------------------------------------------*/
			footer{
				height:50px;
				padding-top:calc((50px - 14px) / 2);
				/*line-height:50px;*/
			}

			/*--------------------------------------------------------
				PAGE STYLISH
			--------------------------------------------------------*/
			body{background:#f5f5f5; }
			main{background:white; }

			footer img{
				border-radius:50%;
				background: white; 
				margin: -0.05em 0.11em 0.05em 0.11em;
			}

			/*--------------------------------------------------------
				TABLE STYLISH
			--------------------------------------------------------*/
			table{margin-top:20px;}

			thead{text-transform:capitalize; }

			tfoot{font-weight:bold; }
			tfoot td:last-child{font-size:125%; } 

			th, td{text-align:center; vertical-align:middle !important; }

			/*
			tr th:not([colspan]):last-child,
			tr td:not([colspan]):last-child{
				background:#D9EDF7;
				font-weight:bold; 
			}
			*/

			/*--------------------------------------------------------
				PURE CSS TOGGLER
			--------------------------------------------------------*/
			/* Hide checkbox */
			.pure-css-toggle{
				position:absolute;
				top:-9999px;
				left:-9999px;
				opacity:0;
				z-index:-9999;
			}

			/* Show / hide rows */
			#menu_main:checked ~ nav .collapse{display:block; }


			/* Change button label */
			#match_only ~ nav label[for="match_only"]:before{font-family:FontAwesome; content:"\f06e"; }
			#match_only:checked ~ nav label[for="match_only"]:before{content:"\f070"; }

			/* Show / hide rows */
			tbody tr:not(.match){display:none; }
			#match_only:checked ~ main tbody tr{display:table-row; }

			/*--------------------------------------------------------
				RESPONSIVE STUFFS
			--------------------------------------------------------*/
			/* Extra Small */
			@media(max-width:767px){
				thead tr:nth-child(2) th span{
					font-size:0 !important;
				}

				th:first-letter{
					font-size:14px !important;
				}
			}

			/* Small */
			@media(min-width:768px){
				nav input{max-width:139px; }
				nav .btn-block span{display:none; }
			}

			/* Medium */
			@media(min-width:992px){
				nav input{max-width:100%; }
			}

			/* Large */
			@media(min-width:1200px){
				nav .btn-block span{display:inline-block; }
			}
		</style>
	</head>

	<body>
		<input type="checkbox" class="pure-css-toggle" id="match_only" checked="checked" />
		<input type="checkbox" class="pure-css-toggle" id="menu_main" />

		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<label for="menu_main" class="navbar-toggle collapsed" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</label>
					<!--
					<a class="navbar-brand" href="<?php echo dirname($_SERVER['PHP_SELF']); ?>">
						Accueil
					</a>
					-->
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<form class="navbar-form">
						<div class="form-group">
							<a class="btn btn-block btn-primary" href="<?php echo dirname($_SERVER['PHP_SELF']); ?>" title="Réinitialiser">
								<i class="fa fa-home"></i>
								<span>
									Accueil
								</span>
							</a>
						</div>
						<div class="form-group">
							<div class="input-group">
								<label for="min" class="input-group-addon">Min</label>
								<input type="number" class="form-control" id="min" name="min" min="<?php echo $_min; ?>" max="<?php echo $_max; ?>" value="<?php echo $min; ?>" />
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<label for="max" class="input-group-addon">Max</label>
								<input type="number" class="form-control" id="max" name="max" min="<?php echo $_min; ?>" max="<?php echo $_max; ?>" value="<?php echo $max; ?>" />
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<label for="query" class="input-group-addon">Num</label>
								<input type="number" class="form-control" id="query" name="query" min="0" max="9" value="<?php echo $query; ?>" />
							</div>
						</div>
						<div class="form-group">
							<button class="btn btn-block btn-primary" title="Lancer la recherche">
								<i class="fa fa-search"></i>
								<span>
									Chercher
								</span>
							</button>
						</div>
						<div class="form-group navbar-right">
							<label for="match_only" class="btn btn-block btn-primary">
								<span>
									Voir
								</span>
							</label>
						</div>
					</form>
				</div>
			</div>
		</nav>

		<main class="container">
			<table class="table table-striped table-bordered table-condensed">
				<thead>
					<tr class="info">
						<th rowspan="2">
							<span>
								Index
							</span>
						</th>
						<?php 

							foreach(array_keys($total) as $y){
								$div_3 	 = floor($y / 3);
								// $mod_3 	 = $y % 3;
								// $_unit 	 = $units[$mod_3];

								if(!isset($_class) || $_class !== $class[$div_3]){
									$_class  = $class[$div_3];
									
									echo '<th colspan="' . (($y % 3) + 1) . '">'; 
									echo '<span>';
									echo $_class;
									echo '</span>';
									echo '</th>'; 
								}
							}

						?>
						<th rowspan="2">
							<span>
								Total
							</span>
						</th>
					</tr>
					<tr class="info">
						<?php 

							foreach(array_keys($total) as $y){
								echo '<th>';
								echo '<span class="small text-muted">';
								echo $units[$y % 3];
								echo '</span>';
								echo '</th>';
							}

						?>
					</tr>
				</thead>
				<tfoot>
					<tr class="info">
						<td class="text-uppercase">
							Totaux
						</td>
						<?php foreach($total as $col) : ?>
							<td>
								<?php echo strrev(implode(' ', str_split((string)strrev($col), 3))); ?>
							</td>
						<?php endforeach; ?>
						<td>
							<?php echo strrev(implode(' ', str_split((string)strrev($final), 3))); ?>
						</td>
					</tr>
				</tfoot>
				<tbody>
					<?php 
						
						// Loop through result to show for each line if the query has been found					
						foreach($result as $i => $current){
							// Detect & set occurence found
							$found = (array_sum($current) > 0) ? true : false;

							// // Exit if none
							// if(!$found) continue;

							// Line opening
							echo '<tr';
							if($found) echo ' class="' . implode(' ', [
								'match',
								// 'info'
							]) . '"';
							echo '>';

							// First cell to show index
							echo '<td>';
							echo strrev(implode(' ', str_split((string)strrev($i), 3))); //str_pad($i, strlen($max), 'X', STR_PAD_LEFT);
							echo '</td>';

							// Loop through columns
							foreach($current as $index){
								echo '<td>';
								echo '<i class="fa fa-';
								echo ($index === 0) ? 'square-o' : 'check-square-o';
								echo '"></i>';
								echo '</td>';
							}

							// Last column showing total from line
							echo '<td>';
							echo array_sum($current);
							echo '</td>';

							// Line closing
							echo '</tr>';
						}
					?>
				</tbody>
			</table>
		</main>

		<footer class="navbar-inverse navbar-fixed-bottom">
			<div class="container">
				<p class="text-muted text-center">
					&copy; <?php echo date('Y'); ?> 
					<a href="https://www.fanchy.fr/" target="_blank" class="avatar avatar--white external" title="Webdeveloper &amp; UI/UX Designer: Websites conception, graphic creations and editorial" rel="nofollow">
						Fanchy Fanch 
						<img src="https://secure.gravatar.com/avatar/3ce046985f27310ecae4bf6b8fa5d204?s=20&amp;d=mm&amp;r=g" alt="Webdeveloper &amp; UI/UX Designer: Websites conception, graphic creations and editorial" class="avatar avatar-20 wp-user-avatar wp-user-avatar-20 photo avatar-default" height="20" width="20">
					</a>
				</p>
			</div>
		</footer>

		<?php if(!preg_match('/localhost/', $_SERVER['HTTP_HOST'])) : ?>
			<script type="text/javascript" src="http://www.fanchy.fr/wp-content/plugins/better-analytics/js/loader.php?ver=1.1.4.js"></script>
		<?php endif; ?>
	</body>
</html>